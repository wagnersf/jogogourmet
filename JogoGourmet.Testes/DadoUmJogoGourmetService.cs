﻿using JogoGourmet.Domain;
using JogoGourmet.Domain.Crosscutting;
using JogoGourmet.Domain.Entities;
using Ninject;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace JogoGourmet.Testes
{
    [TestFixture]
    public class DadoUmJogoGourmetService
    {
        private IJogoGourmetService _jogoGourmetService;

        [SetUp]
        public void SetUp()
        {
            var kernel = Bootstrapper.CreateKernel(new JogoGourmetModule());
            _jogoGourmetService = kernel.Get<IJogoGourmetService>();
        }

        [Test]
        public void PossoAdicionarUmaNovaCategoria()
        {
            var categoria = new Categoria
            {
                Nome = "Categoria 1",
                Pratos = new List<Prato>
                {
                    new Prato
                    {
                        Nome = "Prato 1",
                    }
                }
            };

            _jogoGourmetService.AdicionarNovaCategoria(categoria);
            Assert.AreEqual(3, _jogoGourmetService.GetCateogorias().Count);
        }

        [Test]
        public void PossoAdicionarUmNovoPratoNumaCategoriaExistente()
        {
            var categoria = _jogoGourmetService.GetCateogorias().ToList().First();
            _jogoGourmetService.AdicionarNovoPratoNaCategoria(categoria, new Prato { Nome = "Novo Prato" });

            var pratos = _jogoGourmetService.GetCateogorias().ToList().First().Pratos;

            Assert.AreEqual(2, pratos.Count());
        }

        [Test]
        public void PossoPegarACategoriaSeguinte()
        {
            var categoria = _jogoGourmetService.GetCateogorias().ToList().First();
            var categoriaSeguinte = _jogoGourmetService.GetCategoriaSeguinte(categoria);

            Assert.IsNotNull(categoriaSeguinte);
            Assert.AreNotEqual(categoria, categoriaSeguinte);
        }

        [Test]
        public void ObtenhoNullQuandoTentoBuscarACategoriaSeguinteENaoExiste()
        {
            var categoria = _jogoGourmetService.GetCateogorias().ToList().Last();
            var categoriaSeguinte = _jogoGourmetService.GetCategoriaSeguinte(categoria);

            Assert.IsNull(categoriaSeguinte);
        }

        [Test]
        public void PossoObterUmaCategoriaPorNome()
        {
            var nome = "Massa";
            var categoria = _jogoGourmetService.GetPorNome(nome);

            Assert.AreEqual(nome, categoria.Nome);
        }

        [Test]
        public void ObtenhoNullQuandoBuscoPorNomeEACategoriaNaoExiste()
        {
            var nome = "Doce";
            var categoria = _jogoGourmetService.GetPorNome(nome);

            Assert.IsNull(categoria);
        }

        [Test]
        public void PossoObterOPratoSeguinteDeUmaCategoria()
        {
            var categoria = _jogoGourmetService.GetCateogorias().ToList().First();
            var pratoAdicionado = new Prato { Nome = "Novo Prato" };
            _jogoGourmetService.AdicionarNovoPratoNaCategoria(categoria, pratoAdicionado);
            
            var prato = categoria.Pratos.First();
            var pratoSeguinte = _jogoGourmetService.GetPratoSeguinteDaCategoria(categoria, prato);

            Assert.IsNotNull(pratoSeguinte);
            Assert.AreNotEqual(prato, pratoSeguinte);
            Assert.AreEqual(pratoAdicionado, pratoSeguinte);
        }

        [Test]
        public void ObtenhoNullQuandoTentoBuscarUmPratoSeguinteENaoExiste()
        {
            var categoria = _jogoGourmetService.GetCateogorias().ToList().First();
            var pratoSeguinte = _jogoGourmetService.GetPratoSeguinteDaCategoria(categoria, categoria.Pratos.First());

            Assert.IsNull(pratoSeguinte);
        }
    }
}
