﻿using Ninject;
using Ninject.Modules;

namespace JogoGourmet.Domain.Crosscutting
{
    public class Bootstrapper
    {
        public static IKernel CreateKernel(params INinjectModule[] modules)
            => new StandardKernel(modules);
    }
}
