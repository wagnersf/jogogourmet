﻿using Ninject.Modules;
using Ninject.Extensions.Conventions;
using JogoGourmet.Domain.Entities;
using JogoGourmet.ApplicationServices.Interfaces;
using static System.Reflection.Assembly;

namespace JogoGourmet.Domain.Crosscutting
{
    public class JogoGourmetModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(c => c.From(GetAssembly(typeof(IJogoGourmetService)),
                GetAssembly(typeof(IJogoGourmetApplicationService)))
                .SelectAllClasses()
                .BindDefaultInterfaces()
                .Configure(syntax => syntax.InThreadScope()));
        }
    }
}
