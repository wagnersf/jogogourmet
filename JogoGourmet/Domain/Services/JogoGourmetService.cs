﻿using JogoGourmet.Domain;
using JogoGourmet.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace JogoGourmet.Services
{
    public class JogoGourmetService : IJogoGourmetService
    {
        private readonly IList<Categoria> _categorias;

        public JogoGourmetService()
        {
            _categorias = new List<Categoria>
            {
                new Categoria
                {
                    Nome = "Massa",
                    Pratos = new List<Prato>
                    {
                        new Prato
                        {
                            Nome = "Lasanha",
                        }
                    }
                },                
                new Categoria
                {
                    Pratos = new List<Prato>
                    {
                        new Prato
                        {
                            Nome = "Bolo de Chocolate",
                        }
                    }
                }
            };
        }

        public void AdicionarNovaCategoria(Categoria categoria) => _categorias.Add(categoria);

        public void AdicionarNovoPratoNaCategoria(Categoria categoria, Prato novoPrato)
            => _categorias.FirstOrDefault(c => c.Equals(categoria)).Pratos.Add(novoPrato);

        public Categoria GetCategoriaSeguinte(Categoria categoriaSelecionada)
        {
            var index = GetCateogorias().ToList().FindIndex(p => p.Equals(categoriaSelecionada));
            return _categorias.ElementAtOrDefault(index + 1);
        }

        public IList<Categoria> GetCateogorias() => _categorias;

        public Categoria GetPorNome(string nome)
            => _categorias.FirstOrDefault(c => c.Nome == nome);

        public Prato GetPratoSeguinteDaCategoria(Categoria categoria, Prato pratoSelecionado)
        {
            var pratos = GetPorNome(categoria.Nome).Pratos;
            var index = pratos.ToList().FindIndex(p => p.Equals(pratoSelecionado));
            return pratos.ElementAtOrDefault(index + 1);
        }
    }
}
