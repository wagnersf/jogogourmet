﻿using System.Collections.Generic;

namespace JogoGourmet.Domain
{
    public class Categoria
    {
        public string Nome { get; set; }

        public IList<Prato> Pratos { get; set; }
    }
}
