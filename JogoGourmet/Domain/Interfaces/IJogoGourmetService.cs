﻿using System.Collections.Generic;

namespace JogoGourmet.Domain.Entities
{
    public interface IJogoGourmetService
    {
        IList<Categoria> GetCateogorias();
        Categoria GetCategoriaSeguinte(Categoria categoriaSelecionada);
        Categoria GetPorNome(string nome);
        void AdicionarNovoPratoNaCategoria(Categoria categoria, Prato novoPrato);
        void AdicionarNovaCategoria(Categoria categoria);
        Prato GetPratoSeguinteDaCategoria(Categoria categoria, Prato pratoSelecionado);
    }
}
