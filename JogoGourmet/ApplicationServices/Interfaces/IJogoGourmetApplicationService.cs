﻿using JogoGourmet.Domain;
using System.Collections.Generic;

namespace JogoGourmet.ApplicationServices.Interfaces
{
    public interface IJogoGourmetApplicationService
    {
        IList<Categoria> GetCateogorias();
        Categoria GetCategoriaSeguinte(Categoria categoriaSelecionada);
        Categoria GetPorNome(string text);
        void AdicionarNovoPratoNaCategoria(Categoria categoria, Prato novoPrato);
        void AdicionarNovaCategoria(Categoria categoria);
        Prato GetPratoSeguinteDaCategoria(Categoria categoria, Prato pratoSelecionado);
    }
}
