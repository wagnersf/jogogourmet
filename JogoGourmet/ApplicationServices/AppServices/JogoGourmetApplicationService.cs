﻿using JogoGourmet.ApplicationServices.Interfaces;
using JogoGourmet.Domain;
using JogoGourmet.Domain.Entities;
using System.Collections.Generic;

namespace JogoGourmet.ApplicationServices
{
    public class JogoGourmetApplicationService : IJogoGourmetApplicationService
    {
        private readonly IJogoGourmetService _jogoGourmetService;

        public JogoGourmetApplicationService(IJogoGourmetService jogoGourmetService)
        {
            _jogoGourmetService = jogoGourmetService;
        }

        public void AdicionarNovaCategoria(Categoria categoria)
            => _jogoGourmetService.AdicionarNovaCategoria(categoria);

        public void AdicionarNovoPratoNaCategoria(Categoria categoria, Prato novoPrato)
            => _jogoGourmetService.AdicionarNovoPratoNaCategoria(categoria, novoPrato);

        public Categoria GetCategoriaSeguinte(Categoria categoriaSelecionada)
            => _jogoGourmetService.GetCategoriaSeguinte(categoriaSelecionada);

        public IList<Categoria> GetCateogorias() => _jogoGourmetService.GetCateogorias();

        public Categoria GetPorNome(string nome) => _jogoGourmetService.GetPorNome(nome);

        public Prato GetPratoSeguinteDaCategoria(Categoria categoria, Prato pratoSelecionado)
            => _jogoGourmetService.GetPratoSeguinteDaCategoria(categoria, pratoSelecionado);
    }
}
