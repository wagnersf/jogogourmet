﻿using JogoGourmet.ApplicationServices.Interfaces;
using JogoGourmet.Domain.Crosscutting;
using JogoGourmet.Views;
using Ninject;
using System.Windows;

namespace JogoGourmet
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        private IJogoGourmetApplicationService _jogoGourmet;

        public MainWindow()
        {
            InitializeComponent();
            var kernel = Bootstrapper.CreateKernel(new JogoGourmetModule());
            _jogoGourmet = kernel.Get<IJogoGourmetApplicationService>();
        }

        private void Button_Ok(object sender, RoutedEventArgs e)
        {
            var modalCategoria = new ModalCategoria(_jogoGourmet);
            modalCategoria.ShowDialog();
        }
    }
}
