﻿using JogoGourmet.ApplicationServices.Interfaces;
using JogoGourmet.Domain;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;

namespace JogoGourmet.Views
{
    /// <summary>
    /// Lógica interna para ModalPrato.xaml
    /// </summary>
    public partial class ModalPrato : Window, INotifyPropertyChanged
    {
        private readonly IJogoGourmetApplicationService _jogoGourmetAppService;
        private readonly Categoria _categoria;
        private Prato _pratoSelecionado;

        public event PropertyChangedEventHandler PropertyChanged;

        public string _confirmText;
        public string ConfirmTextPrato
        {
            get => _confirmText;
            set {
                _confirmText = $"O prato que você pensou é {value}?";
                NotifyPropertyChanged("ConfirmTextPrato");
            }
        }

        public ModalPrato(IJogoGourmetApplicationService jogoGourmetAppService, Categoria categoria)
        {
            InitializeComponent();
            DataContext = this;
            _jogoGourmetAppService = jogoGourmetAppService;
            _categoria = categoria;
            _pratoSelecionado = categoria.Pratos.First();
            ConfirmTextPrato = _pratoSelecionado.Nome;
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Button_Sim(object sender, RoutedEventArgs e)
        {
            Close();
            MessageBox.Show("Acertei de Novo!", "Jogo Gourmet");
        }

        private void Button_Nao(object sender, RoutedEventArgs e)
        {
            var prato = _jogoGourmetAppService.GetPratoSeguinteDaCategoria(_categoria, _pratoSelecionado);
            if (prato != null)
            {
                _pratoSelecionado = prato;
                ConfirmTextPrato = _pratoSelecionado.Nome;
                return;
            }

            Close();
            var modalNovoPrato = new ModalNovoPrato(_jogoGourmetAppService, _categoria);
            modalNovoPrato.ShowDialog();
        }
    }
}
