﻿using JogoGourmet.ApplicationServices.Interfaces;
using JogoGourmet.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JogoGourmet.Views
{
    /// <summary>
    /// Lógica interna para ModalNovaCategoria.xaml
    /// </summary>
    public partial class ModalNovaCategoria : Window
    {
        private readonly IJogoGourmetApplicationService _jogoGourmetAppService;
        private readonly Prato _novoPrato;

        public string ConfirmTextCategoria { get; private set; }

        public ModalNovaCategoria(IJogoGourmetApplicationService jogoGourmetAppService, Categoria categoria, Prato novoPrato)
        {
            InitializeComponent();
            DataContext = this;
            _jogoGourmetAppService = jogoGourmetAppService;
            _novoPrato = novoPrato;
            ConfirmTextCategoria = $"{novoPrato.Nome} é _______ mas {categoria.Pratos.Last().Nome} não.";
        }

        private void Button_Ok(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(categoria.Text))
            {
                MessageBox.Show("Digite o nome de uma categoria");
                return;
            }

            var categoriaAdicionada = _jogoGourmetAppService.GetPorNome(categoria.Text);
            if (categoriaAdicionada != null)
                _jogoGourmetAppService.AdicionarNovoPratoNaCategoria(categoriaAdicionada, _novoPrato);
            else
            {
                _jogoGourmetAppService.AdicionarNovaCategoria(new Categoria
                {
                    Nome = categoria.Text,
                    Pratos = new List<Prato> { _novoPrato }
                });
            }
            Close();
        }

        private void Button_Cancelar(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
