﻿using JogoGourmet.ApplicationServices.Interfaces;
using JogoGourmet.Domain;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;

namespace JogoGourmet.Views
{
    /// <summary>
    /// Lógica interna para ModalCategoria.xaml
    /// </summary>
    public partial class ModalCategoria : Window, INotifyPropertyChanged
    {
        private readonly IList<Categoria> _categorias;
        private readonly IJogoGourmetApplicationService _jogoGourmetAppService;
        private Categoria _categoriaSelecionada;

        public event PropertyChangedEventHandler PropertyChanged;

        private string _confirmText;
        public string ConfirmTextCategoria {
            get => _confirmText;
            set {
                _confirmText = $"O prato que você pensou é {value}?";
                NotifyPropertyChanged("ConfirmTextCategoria");
            }
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ModalCategoria(IJogoGourmetApplicationService jogoGourmetAppService)
        {
            InitializeComponent();
            _jogoGourmetAppService = jogoGourmetAppService;
            DataContext = this;
            _categorias = jogoGourmetAppService.GetCateogorias();
            _categoriaSelecionada = _categorias.First();
            ConfirmTextCategoria = _categoriaSelecionada.Nome;
        }

        private void Button_Sim(object sender, RoutedEventArgs e)
        {
            Close();
            if (string.IsNullOrEmpty(_categoriaSelecionada.Nome))
                MessageBox.Show("Acertei de Novo!", "Jogo Gourmet");
            else
            {
                var modalPrato = new ModalPrato(_jogoGourmetAppService, _categoriaSelecionada);
                modalPrato.ShowDialog();
            }
        }

        private void Button_Nao(object sender, RoutedEventArgs e)
        {
            var categoria = _jogoGourmetAppService.GetCategoriaSeguinte(_categoriaSelecionada);
            if (categoria != null)
            {
                _categoriaSelecionada = categoria;
                ConfirmTextCategoria = _categoriaSelecionada.Nome ?? _categoriaSelecionada.Pratos.First().Nome;
                return;
            }

            Close();
            var modalNovoPrato = new ModalNovoPrato(_jogoGourmetAppService, _categoriaSelecionada);
            modalNovoPrato.ShowDialog();
        }
    }
}
