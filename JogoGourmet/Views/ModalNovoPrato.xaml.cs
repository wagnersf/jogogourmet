﻿using JogoGourmet.ApplicationServices.Interfaces;
using JogoGourmet.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace JogoGourmet.Views
{
    /// <summary>
    /// Lógica interna para ModalNovoPrato.xaml
    /// </summary>
    public partial class ModalNovoPrato : Window
    {
        private readonly IJogoGourmetApplicationService _jogoGourmetAppService;
        private readonly Categoria _categoria;

        public ModalNovoPrato(IJogoGourmetApplicationService jogoGourmetAppService, Categoria categoria)
        {
            InitializeComponent();
            _jogoGourmetAppService = jogoGourmetAppService;
            _categoria = categoria;
        }

        private void Button_Ok(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(prato.Text))
            {
                MessageBox.Show("Digite o nome de um prato");
                return;
            }
            Close();
            var novoPrato = new Prato { Nome = prato.Text };

            var modalNovaCategoria = new ModalNovaCategoria(_jogoGourmetAppService, _categoria, novoPrato);
            modalNovaCategoria.ShowDialog();
        }

        private void Button_Cancelar(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
